package re.nobabylone.calli.androidtp1and2.activity.listener;

import android.app.DatePickerDialog.OnDateSetListener;
import android.widget.DatePicker;
import android.widget.TextView;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by callimom on 13/02/16.
 */
public class MyOnDateSetListener implements OnDateSetListener {

    private Calendar calendar;
    private String format;
    private TextView dateTextView;


    /**    -       Constructor      -    **/
    public MyOnDateSetListener(Calendar calendar, TextView dateTextView, String dateFormat) {
        this.calendar = calendar;
        this.format = dateFormat;
        this.dateTextView = dateTextView;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        //**     update the text field     **//
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        dateTextView.setText(sdf.format(calendar.getTime()));
    }
}
