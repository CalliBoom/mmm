package re.nobabylone.calli.androidtp1and2.contentprovider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import re.nobabylone.calli.androidtp1and2.database.MySQLiteHelper;
import re.nobabylone.calli.androidtp1and2.model.Contact;

/**
 * Created by callimom on 07/02/16.
 */
public class ContactContentProvider extends ContentProvider {

    public static final String AUTHORITY = "re.nobabylone.calli.androidtp1and2.provider";
    public static final String SCHEME = "content://";
    private static final String BASE_PATH = "contacts";
    public static final String CONTACTS = SCHEME + AUTHORITY + "/contacts";
    public static final Uri CONTENT_URI = Uri.parse(CONTACTS);
    public static final Uri URI_CONTACTS = Uri.parse(CONTACTS);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/"+BASE_PATH;
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/contact";


    public static final String CONTACT_BASE = CONTACTS + "/";



    @Override
    public boolean onCreate() {
        return true;
    }


    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor resultCursor = null;


        if (URI_CONTACTS.equals(uri))
        {
            resultCursor = MySQLiteHelper
                    .getInstance(getContext())
                    .getWritableDatabase()
                    .query(Contact.TABLE_NAME, projection, selection, selectionArgs, null, null, null, null);

            resultCursor.setNotificationUri(getContext().getContentResolver(), URI_CONTACTS);
        }
        else if (uri.toString().startsWith(CONTACT_BASE))
        {
            String id = uri.getLastPathSegment();


            System.out.println("  - - - -- --- >>>>>>>> " + id +" <<<<<<<<<<<< -  -- -  -- - -");
            System.out.println("  - - - -- --- >>>>>>>> " + id +" <<<<<<<<<<<< -  -- -  -- - -");
            System.out.println("  - - - -- --- >>>>>>>> " + id +" <<<<<<<<<<<< -  -- -  -- - -");
            resultCursor = MySQLiteHelper
                    .getInstance(getContext())
                    .getWritableDatabase()
                    .query(Contact.TABLE_NAME, Contact.FIELDS, Contact.COL_ID + " = ?", new String[] { id }, null, null, null, null);

            resultCursor.setNotificationUri(getContext().getContentResolver(), URI_CONTACTS);
        }
        else
        {
            throw new UnsupportedOperationException("Not yet implemented");
        }

        return resultCursor;

    }


    @Nullable
    @Override
    public String getType(Uri uri) {
        return CONTENT_ITEM_TYPE;
    }


    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = MySQLiteHelper.getInstance(getContext()).getWritableDatabase();
        try {
            long id = db.insertOrThrow(Contact.TABLE_NAME, null, values);

            if (id == -1) {
                throw new RuntimeException(String.format(
                        "%s : Failed to insert [%s] for unknown reasons.","ContactContentProvider", values, uri));
            } else {
                return ContentUris.withAppendedId(uri, id);
            }

        } finally {
            db.close();
        }

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        long id = getId(uri);
        SQLiteDatabase db = MySQLiteHelper.getInstance(getContext()).getWritableDatabase();
        try {
            if (id < 0)
                return db.delete(Contact.TABLE_NAME, selection, selectionArgs);
            else
                return db.delete(Contact.TABLE_NAME, Contact.COL_ID + "=" + id, selectionArgs);
        } finally {
            db.close();
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        long id = getId(uri);
        SQLiteDatabase db = MySQLiteHelper.getInstance(getContext()).getWritableDatabase();

        try {
            if (id < 0)
                return db.update( Contact.TABLE_NAME,values, selection, selectionArgs);
            else
                return db.update( Contact.TABLE_NAME, values, Contact.COL_ID + "=" + id, null);
        } finally {
            db.close();
        }

    }

    private long getId(Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        if (lastPathSegment != null) {
            try {
                return Long.parseLong(lastPathSegment);
            } catch (NumberFormatException e) {

            }
        }
        return -1;
    }


}
