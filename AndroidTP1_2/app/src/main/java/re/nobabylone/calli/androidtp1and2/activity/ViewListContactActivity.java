package re.nobabylone.calli.androidtp1and2.activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import re.nobabylone.calli.androidtp1and2.R;
import re.nobabylone.calli.androidtp1and2.contentprovider.ContactContentProvider;
import re.nobabylone.calli.androidtp1and2.contentprovider.MyCursorAdapter;
import re.nobabylone.calli.androidtp1and2.model.Contact;
import re.nobabylone.calli.androidtp1and2.parcelable.InformationParcelable;

/**
 *
 */
public class ViewListContactActivity extends Activity implements OnItemClickListener {

    /** ListView **/
    private ListView listContactsView;
    private EditText filterField;
    /** Cursor Adapter **/
    CursorAdapter cursorAdapter;

    /** List Contacts **/
    List<Contact> contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list_contact);

/*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/
        /*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  */
        listContactsView = (ListView) findViewById(R.id.listView);
        /*  -  -  -  -  -  -  -  -        On Item Click Listener        -  -  -  -  -  -  -  -  */
        listContactsView.setOnItemClickListener(this);
        /*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  */
        filterField = (EditText) findViewById(R.id.filterField);
        /*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  */
        ContentResolver contentResolver = getContentResolver() ;
        Cursor c = contentResolver.query(ContactContentProvider.CONTENT_URI, null, null, null, null);
        /*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  */
        cursorAdapter = new MyCursorAdapter(this,c, 0);
        /*  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  */
        listContactsView.setAdapter(cursorAdapter);
    }

    @Override
    public void onResume(){
        super.onResume();
        selectAllContactAndPrintToListView();
    }



    /**
     *
     * @param view
     */
    public void addNewContact(View view) {
        Intent intent = new Intent(this, AddContactActivity.class);
        startActivity(intent);
    }

    /**
     * UNUSED : TEST METHOD ADDING ONE CONTACT
     * Static add Contact
     * @param view
     */
    public void addBobMarley(View view) {
        /*    Adding one Contact    */
        Contact contact = new Contact();
        contact.setFirstname("Bob");
        contact.setLastname("Marley");
        contact.setDepartment("Jamaica");

        ContentResolver contentResolver = getContentResolver();
        contentResolver.insert(ContactContentProvider.CONTENT_URI, contact.getContent());

        selectAllContactAndPrintToListView();

    }


    /**
     * UNUSED : TEST METHOD ADDING ONE CONTACT
     * Static add severals contacts
     */
    private void insertSomeStaticContacts() {
        ContentResolver contentResolver = getContentResolver();

        Contact contact = new Contact();
        contact.setLastname("Android");
        contact.setFirstname("Master");
        contentResolver.insert(ContactContentProvider.CONTENT_URI, contact.getContent());

        contact = new Contact();
        contact.setLastname("Angelic");
        contact.setFirstname("Booster");
        contentResolver.insert(ContactContentProvider.CONTENT_URI, contact.getContent());

        contact = new Contact();
        contact.setLastname("Chinois");
        contact.setFirstname("Flingueur");
        contentResolver.insert(ContactContentProvider.CONTENT_URI, contact.getContent());

        selectAllContactAndPrintToListView();
    }


    /**
     * Filter Method
     * @param view
     */
    public void filterContactMethod(View view) {

        String filterKeyWords = filterField.getText().toString();

        if (filterKeyWords.length() != 0) {

            ContentResolver contentResolver = getContentResolver();

            String[] projection = new String[]{Contact.COL_ID, Contact.COL_LASTNAME, Contact.COL_FIRSTNAME, Contact.COL_NUM};
            String selection = new String(Contact.COL_LASTNAME + " LIKE ? OR " + Contact.COL_FIRSTNAME + " LIKE ?" );
            String[] selectionArgs = new String[]{filterKeyWords, filterKeyWords};

            Cursor c = contentResolver.query(ContactContentProvider.CONTENT_URI, projection, selection, selectionArgs, null);

            cursorAdapter.swapCursor(c);
        }
        else {
            selectAllContactAndPrintToListView();
        }

    }


    /**
     * Print all the Contact to the current ListView
     */
    private void selectAllContactAndPrintToListView() {
        ContentResolver contentResolver = getContentResolver();
        Cursor c = contentResolver.query(ContactContentProvider.CONTENT_URI, null, null, null, null);
        cursorAdapter.swapCursor(c);
    }


    /**
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String idString = ((TextView) view.findViewById(R.id.contactID)).getText().toString();
        System.out.println("    - -   - - BLACK OUT - -- - -- - -- - - -- ");
        System.out.println("    "+idString);
        System.out.println("    - -   - - BLACK OUT - -- - -- - -- - - -- ");
        int idInteger = Integer.parseInt(idString);
        viewContact(idInteger);
    }


    /**
     * Launch a new Intent to View the selected contact
     * @param idContact
     */
    private void viewContact(int idContact){

        ContentResolver contentResolver = getContentResolver();
        Uri CONTACTURI = Uri.parse(ContactContentProvider.CONTACT_BASE + idContact);

        System.out.println("  - - - -- --- ?" + CONTACTURI.toString() +"? -  -- -  -- - -");
        System.out.println("  - - - -- --- ?" + CONTACTURI.toString() +"? -  -- -  -- - -");
        System.out.println("  - - - -- --- ?" + CONTACTURI.toString() +"? -  -- -  -- - -");

        Cursor c = contentResolver.query(CONTACTURI, null, null, null, null);

        System.out.println("  - - - -- --- ####" + c.getColumnIndex(Contact.COL_ID) +"#### -  -- -  -- - -");
        System.out.println("  - - - -- --- ####    :" + c.getColumnName(c.getColumnIndex(Contact.COL_ID)) +"#### -  -- -  -- - -");

        String nomText="";
        String prenomText="";
        String department="";
        String birthdate="";
        String birthcity="";
        String numText="";

        while(c.moveToNext()){
            System.out.println("xxxxxxxxxxxxxxxxxxx -->     IndadiClub");

            nomText = c.getString(c.getColumnIndex(Contact.COL_LASTNAME));
            System.out.println(nomText + "   OK");
            prenomText = c.getString(c.getColumnIndex(Contact.COL_FIRSTNAME));
            System.out.println(prenomText + "   OK");
            department = c.getString(c.getColumnIndex(Contact.COL_DEPARTMENT));
            birthdate = c.getString(c.getColumnIndex(Contact.COL_BIRTHDATE));
            birthcity = c.getString(c.getColumnIndex(Contact.COL_BIRTHCITY));
            numText = c.getString(c.getColumnIndex((Contact.COL_NUM)));
        }

        //Toast.makeText(getApplicationContext(),department + " ;" + birthcity + " en " + birthdate, Toast.LENGTH_LONG);

        InformationParcelable parcelable = new InformationParcelable(nomText,prenomText,birthdate,birthcity,department,numText);
        Intent intent = new Intent(this, ViewOneContactActivity.class);
        intent.putExtra("parcel", parcelable);
        startActivity(intent);
    }

}


