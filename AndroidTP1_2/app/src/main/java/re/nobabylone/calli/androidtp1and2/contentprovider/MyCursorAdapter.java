package re.nobabylone.calli.androidtp1and2.contentprovider;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import re.nobabylone.calli.androidtp1and2.R;
import re.nobabylone.calli.androidtp1and2.model.Contact;

/**
 * Created by callimom on 07/02/16.
 */
public class MyCursorAdapter extends CursorAdapter {

    private LayoutInflater mInflater;

    /**     Constructor     **/
    public MyCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.item, parent, false);
        return retView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ImageView imageView = (ImageView) view.findViewById(R.id.iconContact);
        if ( cursor.getPosition() % 2 == 0) {
            //view.setBackgroundColor(Color.LTGRAY);
            imageView.setImageResource(R.drawable.contact1);
        }
        else {
            //view.setBackgroundColor(Color.WHITE);
            imageView.setImageResource(R.drawable.contact2);
        }

        TextView names = (TextView) view.findViewById(R.id.itemNomPrenom);
        String lastname = cursor.getString(cursor.getColumnIndex(Contact.COL_LASTNAME));
        String firstname = cursor.getString(cursor.getColumnIndex(Contact.COL_FIRSTNAME));
        names.setText(lastname + " " + firstname);

        TextView id = (TextView) view.findViewById(R.id.contactID);
        id.setText(cursor.getString(cursor.getColumnIndex(Contact.COL_ID)));

        TextView num = (TextView) view.findViewById(R.id.itemNumero);
        num.setText(cursor.getString(cursor.getColumnIndex(Contact.COL_NUM)));
    }


}

