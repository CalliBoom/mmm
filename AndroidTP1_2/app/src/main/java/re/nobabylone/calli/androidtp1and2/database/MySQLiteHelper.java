package re.nobabylone.calli.androidtp1and2.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import re.nobabylone.calli.androidtp1and2.model.Contact;

/**
 * Created by callimom on 07/02/16.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    /**   -   Static Constants   -  **/
    public static final String DATABASE_NAME = "contactdatabase.db";
    private static final int DATABASE_VERSION = 2;


    private final Context context;
    private static MySQLiteHelper singleton;

    /**
     *
     * @param context
     * @return
     */
    public static MySQLiteHelper getInstance(final Context context) {

        if (singleton == null) {
            singleton = new MySQLiteHelper(context);
        }
        return singleton;
    }


    /**
     * Constructor
     * @param context
     */
    private MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context.getApplicationContext();
    }

    /**
     * On create Method : Just create a Our Contact Database
     * @param database
     */
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(Contact.CREATE_TABLE);
    }


    /**
     * On Upgrade : Delete the database and Create a new one.
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + Contact.TABLE_NAME);
        onCreate(db);
    }

}