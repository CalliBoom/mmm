package re.nobabylone.calli.androidtp1and2.activity;

import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;


import re.nobabylone.calli.androidtp1and2.R;
import re.nobabylone.calli.androidtp1and2.activity.listener.MyOnDateSetListener;
import re.nobabylone.calli.androidtp1and2.contentprovider.ContactContentProvider;
import re.nobabylone.calli.androidtp1and2.model.Contact;
import re.nobabylone.calli.androidtp1and2.parcelable.InformationParcelable;

public class AddContactActivity extends AppCompatActivity {

    /**    -   TextFields   -    **/
    private EditText lastname, firstname, birthcity, num;
    private TextView birthdate;
    /**    -   TextFields   -    **/
    private Calendar birthdateCalendar;
    /**    -   List of departments   -    **/
    private Spinner departments;

    /**    -   Layout   -    **/
    private LinearLayout linearLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        /*   -    Initialization of the controls    -   */
        lastname = (EditText) findViewById(R.id.lastname);
        firstname = (EditText) findViewById(R.id.firstname);
        birthdate = (TextView) findViewById(R.id.birthdate);
        birthcity = (EditText) findViewById(R.id.birthcity);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        departments = (Spinner) findViewById(R.id.department);
        num = null;
        birthdateCalendar = Calendar.getInstance();
        birthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(
                        AddContactActivity.this,
                        new MyOnDateSetListener(birthdateCalendar, birthdate, "mm/dd/yy"),
                        birthdateCalendar.get(Calendar.YEAR),
                        birthdateCalendar.get(Calendar.MONTH),
                        birthdateCalendar.get(Calendar.DAY_OF_MONTH)
                ).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.menuRAZ:
                razAction();
                return true;

            case R.id.addNumber:
                addPhoneNumberView();
                return true;

            case R.id.wiki:
                openWikipediaSearchDepartment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Launch a new Intent : Wikipedia Home Page
     */
    private void openWikipediaSearchDepartment() {
        String dpt = departments.getSelectedItem().toString();
        String lang = Locale.getDefault().getLanguage();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+lang+".wikipedia.org/wiki/"+dpt));
        startActivity(intent);
    }

    /**
     * Add a new TextField to our layout
     * Number textfield.
     */
    private void addPhoneNumberView() {
        if(num == null) {
            num = new EditText(this);
            linearLayout.addView(num);
            num.setHint(R.string.hintNumber);
            num.setInputType(InputType.TYPE_CLASS_PHONE);
        }
    }



    /**
     * Clean all the Text Fields
     */
    private void razAction(){
        lastname.setText("");
        firstname.setText("");
        birthdate.setText("");
        birthcity.setText("");
        if(num != null){
            num.setText("");
        }
    }


    /**
     * Exercice 1
     * Print the User's informations to the screen through a Toast.
     * @param view
     */
    public void validateButton(View view){
        String printToScreen = "Vous vous appelez : " + lastname.getText() + " " + firstname.getText() +
                ". Etes né(e) le : " + birthdate.getText() + " à " + birthcity.getText() + ". ";

        String dpt = departments.getSelectedItem().toString();
        if ( ! dpt.equals("None") ){
            printToScreen += " (Departement: "+ dpt + ") ";
        }
        if ( num != null) {
            printToScreen += "\nVotre Numero: "+ num.getText() ;
        }
        Toast.makeText(getApplicationContext(),
                printToScreen, Toast.LENGTH_SHORT).show();
    }


    /**
     * Exercice 2.
     * Launch a new screen (ViewOneContactActivity) through the standard Intent technique
     * @param view
     */
    public void viewThisContactSimpleIntent(View view){
        Intent intent = new Intent(this, ViewOneContactActivity.class);
        intent.putExtra("lastname", lastname.getText().toString());
        intent.putExtra("firstname", firstname.getText().toString());
        intent.putExtra("birthdate", birthdate.getText().toString());
        intent.putExtra("birthcity", birthcity.getText().toString());
        intent.putExtra("departement", departments.getSelectedItem().toString());
        if ( num != null) {
            intent.putExtra("num", num.getText().toString());
        }
        startActivity(intent);
    }



    /**
     * Exercice 3.
     * Launch a new screen (ViewOneContactActivity) through a Parcelable
     * @param view
     */
    public void viewThisContactWithParcelable(View view){
        String nomText = lastname.getText().toString();
        String prenomText = firstname.getText().toString();
        String birthdateText = birthdate.getText().toString();
        String birthcityText = birthcity.getText().toString();
        String departementText = departments.getSelectedItem().toString();
        String numText = "";
        if ( num != null) {
            numText = num.getText().toString();
        }

        InformationParcelable parcelable = new InformationParcelable(nomText,prenomText,birthdateText,birthcityText,departementText,numText);
        Intent intent = new Intent(this, ViewOneContactActivity.class);
        intent.putExtra("parcel", parcelable);
        startActivity(intent);
    }

    /**
     * TP 2.
     * Save new Contact in MySQLite DataBase
     * @param view
     */
    public void validateButtonSaveInMySQLite(View view){

        String nomText = lastname.getText().toString();
        String prenomText = firstname.getText().toString();
        String birthdateText = birthdate.getText().toString();
        String birthcityText = birthcity.getText().toString();
        String departementText = departments.getSelectedItem().toString();
        String numText = "";
        if ( num != null){
            numText = num.getText().toString();
        }

        Contact newContact = new Contact(nomText, prenomText, departementText, birthdateText, birthcityText, numText);
        ContentResolver contentResolver = getContentResolver() ;
        contentResolver.insert(ContactContentProvider.CONTENT_URI, newContact.getContent());

        AddContactActivity.this.finish();
        //Intent intent = new Intent(this, ViewListContactActivity.class);
        //startActivity(intent);
    }


}
