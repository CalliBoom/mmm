package re.nobabylone.calli.androidtp1and2.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mickael Callimoutou on 18/01/16.
 * This class is use to transport Users Information from the AddContactActivity to the ViewOneContactActivity
 */
public class InformationParcelable implements Parcelable {

    /**  -   Attributes  -  **/
    String nom;
    String prenom;
    String birthdate;
    String birthcity;
    String departement;
    String num;

    /**  -   Standard Constructor  -  **/
    public InformationParcelable(String nom, String prenom, String birthdate, String birthcity, String departement, String num){
        this.nom = nom;
        this.prenom = prenom;
        this.birthdate = birthdate;
        this.birthcity = birthcity;
        this.departement = departement;
        this.num = num;
    }

    /**  -   Reads In Constructor  -  **/
    protected InformationParcelable(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<InformationParcelable> CREATOR = new Creator<InformationParcelable>() {
        @Override
        public InformationParcelable createFromParcel(Parcel in) {
            return new InformationParcelable(in);
        }

        @Override
        public InformationParcelable[] newArray(int size) {
            return new InformationParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    /**  -   Write To Parcel   -  **/
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeString(prenom);
        dest.writeString(birthdate);
        dest.writeString(birthcity);
        dest.writeString(departement);
        dest.writeString(num);
    }

    /**  -   Read From Parcel -  **/
    private void readFromParcel(Parcel in) {
        nom = in.readString();
        prenom = in.readString();
        birthcity = in.readString();
        birthdate = in.readString();
        departement= in.readString();
        num = in.readString();
    }

    /**  -   Getters   -  **/
    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getBirthcity() {
        return birthcity;
    }

    public String getDepartement() {
        return departement;
    }

    public String getNum() {
        return num;
    }
}