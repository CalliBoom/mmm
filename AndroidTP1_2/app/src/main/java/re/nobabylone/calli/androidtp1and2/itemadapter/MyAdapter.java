package re.nobabylone.calli.androidtp1and2.itemadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import re.nobabylone.calli.androidtp1and2.R;

/**
 * Created by callimom on 19/01/16.
 */

public class MyAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public MyAdapter(Context context, String[] values) {
        super(context, R.layout.item, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item, parent, false);

        TextView nomprenomView = (TextView) rowView.findViewById(R.id.itemNomPrenom);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.iconContact);

        nomprenomView.setText(values[position]);

        // Alternate Logo Contact
        if (position%2==0) {
            imageView.setImageResource(R.drawable.contact1);
        } else {
            imageView.setImageResource(R.drawable.contact2);
        }

        return rowView;
    }
}