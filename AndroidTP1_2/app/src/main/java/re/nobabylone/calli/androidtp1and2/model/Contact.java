package re.nobabylone.calli.androidtp1and2.model;

import android.content.ContentValues;

/**
 * Created by callimom on 07/02/16.
 */
public class Contact {


    public static final String COL_ID = "_id";
    public static final String COL_LASTNAME = "lastname";
    public static final String COL_FIRSTNAME = "firstname";
    public static final String COL_DEPARTMENT = "department";
    public static final String COL_BIRTHDATE = "birthdate";
    public static final String COL_BIRTHCITY = "birthcity";
    public static final String COL_NUM = "num";


    // For database projection so order is consistent
    public static final String[] FIELDS = { COL_ID, COL_FIRSTNAME, COL_LASTNAME,
            COL_DEPARTMENT, COL_BIRTHDATE, COL_BIRTHCITY, COL_BIRTHDATE, COL_NUM };

    public static final String TABLE_NAME = "Contact";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COL_ID + " INTEGER PRIMARY KEY,"
                    + COL_FIRSTNAME + " TEXT NOT NULL DEFAULT '',"
                    + COL_LASTNAME + " TEXT NOT NULL DEFAULT '',"
                    + COL_DEPARTMENT + " TEXT NOT NULL DEFAULT '',"
                    + COL_BIRTHDATE + " TEXT NOT NULL DEFAULT '',"
                    + COL_BIRTHCITY + " TEXT NOT NULL DEFAULT '',"
                    + COL_NUM + " TEXT NOT NULL DEFAULT ''"
                    + ")";



    /**  -         *     Attributes     *        -  **/
    private long id;
    /**  -         *  *        -  **/
    private String lastname;
    private String firstname;
    private String department;
    private String birthdate;
    private String birthcity;
    private String num;



    /**  -         * Constructors *        -  **/
    public Contact() {}

    public Contact(String lastname, String firstname, String department, String birthdate, String birthcity) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.birthdate = birthdate;
        this.birthcity = birthcity;
        this.department = department;
    }

    public Contact(String lastname, String firstname, String department, String birthdate, String birthcity, String num) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.birthdate = birthdate;
        this.birthcity = birthcity;
        this.department = department;
        this.num = num;
    }

    /**  -         * Getter  Content  Values*        -  **/
    public ContentValues getContent() {
        final ContentValues values = new ContentValues();
        // Note that ID is NOT included here
        values.put(COL_FIRSTNAME, firstname);
        values.put(COL_LASTNAME, lastname);
        values.put(COL_DEPARTMENT, department);
        values.put(COL_BIRTHDATE, birthdate);
        values.put(COL_BIRTHCITY, birthcity);
        values.put(COL_NUM, num);

        return values;
    }

    /**  -         * Getters and Setters *        -  **/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthcity() {
        return birthcity;
    }

    public void setBirthcity(String birthcity) {
        this.birthcity = birthcity;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }


}
