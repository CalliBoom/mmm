package re.nobabylone.calli.androidtp1and2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import re.nobabylone.calli.androidtp1and2.R;
import re.nobabylone.calli.androidtp1and2.parcelable.InformationParcelable;

public class ViewOneContactActivity extends AppCompatActivity {

    /**    -   TextFields   -    **/
    TextView nom, prenom, birthdate, birthcity, departement, num;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secund);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        nom = (TextView) findViewById(R.id.idNom);
        prenom = (TextView) findViewById(R.id.idPrenom);
        birthcity = (TextView) findViewById(R.id.idBirthCity);
        birthdate = (TextView) findViewById(R.id.idBirthDate);
        departement = (TextView) findViewById(R.id.idDepartement);
        num = (TextView) findViewById(R.id.idNum);

        /**   -  Intent Simple  -   **/

        /**   -  Intent Parcelable  -   **/

        Bundle b = getIntent().getExtras();
        InformationParcelable contactParcelable = b.getParcelable("parcel");
        if(contactParcelable != null) {
            nom.setText(contactParcelable.getNom());
            prenom.setText(contactParcelable.getPrenom().toString());
            birthcity.setText(contactParcelable.getBirthcity().toString());
            birthdate.setText(contactParcelable.getBirthdate().toString());
            departement.setText(contactParcelable.getDepartement().toString());
            num.setText(contactParcelable.getNum().toString());
        }
        else {

            Intent intent = getIntent();
            nom.setText(intent.getStringExtra("lastname"));
            prenom.setText(intent.getStringExtra("firstname"));
            birthcity.setText(intent.getStringExtra("birthcity"));
            birthdate.setText(intent.getStringExtra("birthdate"));
            departement.setText(intent.getStringExtra("departement"));
            num.setText(intent.getStringExtra("num"));
        }

    }

}
